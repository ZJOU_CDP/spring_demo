package com.zjou.cdp.pojo;

public class Qualification {
    private Integer qualificationId;
    private String qualificationName;

    public Qualification() {
    }

    @Override
    public String toString() {
        return "Qualification{" +
                "qualificationId=" + qualificationId +
                ", qualificationName='" + qualificationName + '\'' +
                '}';
    }

    public Integer getQualificationId() {
        return qualificationId;
    }

    public void setQualificationId(Integer qualificationId) {
        this.qualificationId = qualificationId;
    }

    public String getQualificationName() {
        return qualificationName;
    }

    public void setQualificationName(String qualificationName) {
        this.qualificationName = qualificationName;
    }
}
