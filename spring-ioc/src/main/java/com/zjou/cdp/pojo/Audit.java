package com.zjou.cdp.pojo;

public class Audit {
    private Integer auditId;
    private State stateId;
    private String reason;
    private Company companyId;

    @Override
    public String toString() {
        return "Audit{" +
                "auditId=" + auditId +
                ", stateId=" + stateId +
                ", reason='" + reason + '\'' +
                ", companyId=" + companyId +
                '}';
    }

    public Audit() {
    }

    public Integer getAuditId() {
        return auditId;
    }

    public void setAuditId(Integer auditId) {
        this.auditId = auditId;
    }

    public State getStateId() {
        return stateId;
    }

    public void setStateId(State stateId) {
        this.stateId = stateId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Company getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Company companyId) {
        this.companyId = companyId;
    }
}
