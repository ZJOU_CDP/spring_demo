package com.zjou.cdp.pojo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.sql.Date;

@Component
public class Company {
    @Value("#{values.companyId}")
    private Integer companyId;
    @Value("#{values.companyName}")
    private String companyName;
    @Autowired
    private Person personId;
    private Date dateOfEstablishment;
    private String password;
    private Person legalPersonId;
    private String fax;
    private String companyPhone;
    private String postcode;
    private String address;
    private String registeredFund;
    private String license;
    private String competentDepartment;
    private String securityClearanceNumber;
    private String licensePath;
    private String securityClearancePath;
    private Contractors contractorsId;
    private Qualification qualificationId;
    private String companyEmail;

    public String getCompanyEmail() {
        return companyEmail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Company)) return false;

        Company company = (Company) o;

        return getCompanyName() != null ? getCompanyName().equals(company.getCompanyName()) : company.getCompanyName() == null;

    }

    @Override
    public int hashCode() {
        return getCompanyName() != null ? getCompanyName().hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Company{" +
                "companyId=" + companyId +
                ", companyName='" + companyName + '\'' +
                ", personId=" + personId +
                ", dateOfEstablishment=" + dateOfEstablishment +
                ", password='" + password + '\'' +
                ", legalPersonId=" + legalPersonId +
                ", fax='" + fax + '\'' +
                ", companyPhone='" + companyPhone + '\'' +
                ", postcode='" + postcode + '\'' +
                ", address='" + address + '\'' +
                ", registeredFund='" + registeredFund + '\'' +
                ", license='" + license + '\'' +
                ", competentDepartment='" + competentDepartment + '\'' +
                ", securityClearanceNumber='" + securityClearanceNumber + '\'' +
                ", licensePath='" + licensePath + '\'' +
                ", securityClearancePath='" + securityClearancePath + '\'' +
                ", contractorsId=" + contractorsId +
                ", qualificationId=" + qualificationId +
                ", companyEmail='" + companyEmail + '\'' +
                '}';
    }

    public void setCompanyEmail(String companyEmail) {
        this.companyEmail = companyEmail;
    }

    public Contractors getContractorsId() {
        return contractorsId;
    }

    public void setContractorsId(Contractors contractorsId) {
        this.contractorsId = contractorsId;
    }

    public Qualification getQualificationId() {
        return qualificationId;
    }

    public void setQualificationId(Qualification qualificationId) {
        this.qualificationId = qualificationId;
    }

    public Date getDateOfEstablishment() {
        return dateOfEstablishment;
    }

    public void setDateOfEstablishment(Date dateOfEstablishment) {
        this.dateOfEstablishment = dateOfEstablishment;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Person getLegalPersonId() {
        return legalPersonId;
    }

    public void setLegalPersonId(Person legalPersonId) {
        this.legalPersonId = legalPersonId;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getCompanyPhone() {
        return companyPhone;
    }

    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRegisteredFund() {
        return registeredFund;
    }

    public void setRegisteredFund(String registeredFund) {
        this.registeredFund = registeredFund;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getCompetentDepartment() {
        return competentDepartment;
    }

    public void setCompetentDepartment(String competentDepartment) {
        this.competentDepartment = competentDepartment;
    }

    public String getSecurityClearanceNumber() {
        return securityClearanceNumber;
    }

    public void setSecurityClearanceNumber(String securityClearanceNumber) {
        this.securityClearanceNumber = securityClearanceNumber;
    }

    public String getLicensePath() {
        return licensePath;
    }

    public void setLicensePath(String licensePath) {
        this.licensePath = licensePath;
    }

    public String getSecurityClearancePath() {
        return securityClearancePath;
    }

    public void setSecurityClearancePath(String securityClearancePath) {
        this.securityClearancePath = securityClearancePath;
    }

    public Company() {
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Person getPersonId() {
        return personId;
    }

    public void setPersonId(Person personId) {
        this.personId = personId;
    }
}
