package com.zjou.cdp.pojo;

public class Contractors {
    private Integer contractorsId;
    private String contractorsName;

    @Override
    public String toString() {
        return "Contractors{" +
                "contractorsId=" + contractorsId +
                ", contractorsName='" + contractorsName + '\'' +
                '}';
    }

    public Contractors() {
    }

    public Integer getContractorsId() {
        return contractorsId;
    }

    public void setContractorsId(Integer contractorsId) {
        this.contractorsId = contractorsId;
    }

    public String getContractorsName() {
        return contractorsName;
    }

    public void setContractorsName(String contractorsName) {
        this.contractorsName = contractorsName;
    }
}
