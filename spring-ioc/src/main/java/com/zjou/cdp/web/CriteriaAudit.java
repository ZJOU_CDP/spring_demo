package com.zjou.cdp.web;

public class CriteriaAudit {
    private String companyName;
    private String personName;
    private Integer stateId;
    private String phone;
    private Integer pageNo;

    //初始化的回调方法
    public void init(){
        System.out.println("初始化的回调方法");
    }

    //销毁的回调方法
    public void destroy(){
        System.out.println("销毁的回调方法");
    }

    @Override
    public String toString() {
        return "CriteriaAudit{" +
                "companyName='" + companyName + '\'' +
                ", personName='" + personName + '\'' +
                ", stateId=" + stateId +
                ", phone='" + phone + '\'' +
                ", pageNo=" + pageNo +
                '}';
    }

    public CriteriaAudit() {
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName.equals("") ? null : companyName;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName.equals("") ? null : personName;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId == -1 ? null : stateId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone.equals("") ? null : phone;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }
}
