package com.zjou.cdp;

import com.zjou.cdp.pojo.Audit;
import com.zjou.cdp.pojo.Company;
import com.zjou.cdp.pojo.Person;
import com.zjou.cdp.web.CriteriaAudit;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class Application {

    private Person person;

    @Resource
    public void setPerson(Person person) {
        this.person = person;
    }

    @Test
    public void test1() {

        ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
        //作用域为 prototype的bean
        CriteriaAudit criteriaAudit = context.getBean("criteriaAudit", CriteriaAudit.class);
        CriteriaAudit criteriaAudit1 = context.getBean("criteriaAudit", CriteriaAudit.class);
        System.out.println(criteriaAudit == criteriaAudit1);

        //作用域为singleton的bean
        CriteriaAudit criteriaAudit2 = context.getBean("criteriaAudit1", CriteriaAudit.class);
        CriteriaAudit criteriaAudit13 = context.getBean("criteriaAudit1", CriteriaAudit.class);
        System.out.println(criteriaAudit2 == criteriaAudit13);

    }

    @Test
    public void test3() {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
        Audit audit = context.getBean("audit", Audit.class);
        System.out.println(audit);
    }

    @Test
    public void testAnno(){
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-anno.xml");
        Company company = context.getBean("company", Company.class);
        System.out.println(company);
    }
}
