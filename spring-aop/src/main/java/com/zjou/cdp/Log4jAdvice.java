package com.zjou.cdp;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class Log4jAdvice {

    //切入点
    @Pointcut("execution(* com.zjou.cdp.Service.*(..))")
    public void cut() {

    }


    @Before("cut()")
    public void before(JoinPoint joinPoint) {
        Class aClass = joinPoint.getTarget().getClass();
        //日志类
        Logger logger = Logger.getLogger(aClass);
        //简单类名
        String classSimpleName = aClass.getSimpleName();
        //参数列表
        Object[] args = joinPoint.getArgs();
        StringBuilder argsStr = new StringBuilder();
        if (args.length > 0) {
            for (int i = 0; i < args.length - 1; i++) {
                argsStr.append(args[i]).append(",");
            }
            argsStr.append(args[args.length - 1]);
        }
        //方法名
        Signature signature = joinPoint.getSignature();
        String methodName = signature.getName();
        //打印日志
        logger.debug(classSimpleName + "-->" + methodName + "(" + (args.length > 0 ? argsStr : "") + ")");
    }

    //异常日志：类名，方法名，参数列表，抛出的异常类，异常消息
    @AfterThrowing(value = "cut()", throwing = "ex")
    public void exception(JoinPoint joinPoint, Throwable ex) {
        Class aClass = joinPoint.getTarget().getClass();
        //日志类
        Logger logger = Logger.getLogger(aClass);
        //简单类名
        String classSimpleName = aClass.getSimpleName();
        //参数列表
        Object[] args = joinPoint.getArgs();
        StringBuilder argsStr = new StringBuilder();
        if (args.length > 0) {
            for (int i = 0; i < args.length - 1; i++) {
                argsStr.append(args[i]).append(",");
            }
            argsStr.append(args[args.length - 1]);
        }
        //方法名
        Signature signature = joinPoint.getSignature();
        String methodName = signature.getName();

        //打印日志
        logger.error(classSimpleName + "-->" + methodName + "(" + (args.length > 0 ? argsStr : "") + ")" + "-----" + ex.getClass().getName() + "-->" + ex.getMessage());
    }

    @AfterReturning(value = "cut()", returning = "returnValue")
    public void returned(JoinPoint joinPoint, Object returnValue) {
        Class aClass = joinPoint.getTarget().getClass();
        //日志类
        Logger logger = Logger.getLogger(aClass);
        //简单类名
        String classSimpleName = aClass.getSimpleName();
        //参数列表
        Object[] args = joinPoint.getArgs();
        StringBuilder argsStr = new StringBuilder();
        if (args.length > 0) {
            for (int i = 0; i < args.length - 1; i++) {
                argsStr.append(args[i]).append(",");
            }
            argsStr.append(args[args.length - 1]);
        }
        //方法名
        Signature signature = joinPoint.getSignature();
        String methodName = signature.getName();
        //返回值
        //打印日志
        logger.debug(classSimpleName + "-->" +
                methodName + "(" + (args.length > 0 ? argsStr : "") + ")" +
                (returnValue != null ? (" return " + returnValue) : " void"));
    }


}
