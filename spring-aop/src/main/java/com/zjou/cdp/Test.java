package com.zjou.cdp;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {
    public static void main(String[] args) throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
        Service service = context.getBean("service",Service.class);
        service.service1();
        double v = service.service2(1, 4);
    }
}
