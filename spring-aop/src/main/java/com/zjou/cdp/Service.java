package com.zjou.cdp;

import org.springframework.stereotype.Component;

@Component
public class Service {
    public void service1() {
        System.out.println("业务逻辑1");
    }

    public double service2(int a, int b) throws Exception {
        int c = a + b;
        if (c < 0) {
            throw new Exception("开根号错误");
        }
        return Math.sqrt(a + b);
    }

}
